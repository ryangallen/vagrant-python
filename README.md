# Vagrant Python

A clean Python 3 development environment. This project can be used as a starting
point for any Python project where developers will collaborate and need a
consistent development environment that is easy to set up.

## What's in the box?

- [`debian/contrib-stretch64`](https://app.vagrantup.com/debian/boxes/contrib-stretch64)
  virtual box OS (`contrib-*` boxes include the `vboxfs` kernel module for shared folders)
- [Python 3.5](https://www.python.org/)
- [Pipenv](https://pipenv.readthedocs.io/en/latest/)

## Set up

1. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and
   [Vagrant](https://www.vagrantup.com/downloads.html)
1. Clone this repository and launch the VM:

        $ git clone git@<repository_host>:vagrant-python.git <project_name>
        $ cd <project_name>
        $ vagrant up

## Development

- Use `vagrant ssh` to access the VM shell
- [Pipenv](https://pypi.org/project/pipenv/) is already installed on the VM and
  ready to use. You do not need to install or use virtualenv/virtualenvwrapper
- Use `pipenv run` to run a given command from the virtualenv, with any
  arguments forwarded
- Use `pipenv install [OPTIONS] [PACKAGE_NAME]` to install additional packages
  in the virtualenv

### Environment variables

A `.env` file is created when you first call `vagrant up` and can be
edited to match your project specifications. Pipenv loads the variables
automatically when you call `pipenv run`.

### Example Usage

To install and use the Requests library:

    $ pipenv install requests
    Creating a Pipfile for this project…
    ...
    $ pipenv run python
    >>> import requests
    >>> resp = requests.get('https://httpbin.org/ip')
    >>> print('My IP is {ip}'.format(ip=resp.json().get('origin')))
