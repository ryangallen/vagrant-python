# !/bin/bash

export DEBIAN_FRONTEND=noninteractive

sudo -E apt-get update
sudo -E apt-get install python3-dev -y python3-pip

python3 -m pip install --user pipenv

# Set up environment
if [ ! -f /vagrant/.env ]; then
  cp /vagrant/conf/template.env /vagrant/.env
fi
sudo ln -sf /vagrant/conf/.bashrc /home/vagrant/.

# Set up environment vars
export PATH=$PATH:/home/vagrant/.local/bin
export PIPENV_VENV_IN_PROJECT=true

cd /vagrant
pipenv install
